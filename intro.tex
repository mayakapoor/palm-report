\section{Introduction}

The scanning of packets and their payloads plays a vital role in network security in combating malware, data leakage, content and rights violations, illegal content distribution, and terrorist, criminal, and mis-information spread. In order to accomplish these goals, network operators and inspectors must be able to identify particular applications or protocols that packets belong to while preserving users' rights to privacy and integrity of the data.

Because of the nature of protocol standards and application programming interfaces, payloads often contain commonalities or patterns which may indicate what application or protocol they belong to. Features from the packet header as well as the data layers themselves may also contain information that indicate network architecture (tunneling, for example), encryption type, and/or compression. All these details can be incredibly useful for a variety of cyber security applications.

Network engineers have adapted regular expressions for searching for identifiable patterns in packets. For example, an administrator may have a set of signatures $R=\{r_1;r_2;r_3\}$ where $r_1=\text{"GET*HTTP\slash1.1\textbackslash r\textbackslash n"}$, $r_2=\text{"POST*HTTP\slash1.1\textbackslash r \textbackslash n"}$, and $r_3=\text{"HEAD*HTTP\slash1.1\textbackslash r\textbackslash n"}$ to match a subset of HTTP request traffic. Real signatures are often more complex than this, and protocols or applications may have multiple signatures to match request, response, or message types. Furthermore, different versions of protocols or updated standards and RFCs often require separate signature sets and the continual update of older patterns.

Regular expression scanning is a notoriously slow operation that becomes exponentially worse as the complexity and number of regular expressions increases. With the increasing size and complexity of rulesets and the growing scale of the network environment, regular expression scanning is being rendered more and more impractical. As a forward-thinking solution, we propose \textsc{PALM-Forest}, a system for \textbf{P}rotocol \textbf{A}utodetection using \textbf{L}ocality-Sensitive \textbf{M}ethods. To our knowledge, \textsc{PALM-Forest} is a novel approach to multi-class protocol identification across such a variety of packet types as we show in our experiments. This method provides the following contributions to the state of the art:

\begin{itemize}
\item A process for generating locality-sensitive hashes from packets which is highly accurate for protocol identification,
\item The ability to distinguish traffic classes like non-encrypted traffic from particular flows or streams,
\item An alternative to regular expression scanning for DPI which scales sublinearly and requires a linear amount of storage space,
\item A set of feature isolation tests to analyze what potential features are useful for certain protocols for identification,
\item A diverse and unique application of the traffic classification problem with experiments classifying multiple classes of protocols across many traffic types,
\item and public datasets and source code for experimental reproducibility.
\end{itemize}
