\section{Methodology}

We use the 3-tuple information (source and destination ports and transport-layer protocol) as well as flag values and packet length to make up one set of tokens. Then, \textsc{PALM-Forest} delimits packet payloads by whitespace in order to create word-style tokens. In future work, other delimiters or a sliding-window shingling technique may suit identification of certain data types such as binary application layer protocols like HTTP2. In the experiments presented in this report, whitespace proved to be the most effective delimiter.

MinHash was used to generate the locality-sensitive hashes for the data samples and test samples. We use the LSHForest algorithm developed by Bawa et al~\cite{lshforest} to index the locality-sensitive hashes and optimize the search process. It improves upon nearest-neighbor searches by both complexity and eliminating the need to know the distance $r$ from the query to the nearest neighbor of a given data point. This optimization is achieved through the use of a specific set of hash functions which ensures that any nearest neighbor query returns $\epsilon$-approximate neighbors so long as a suitable $l$ number of trees is chosen.

\medskip

\begin{definition}
\textit{~\cite{lshforest} A family $H$ of functions from a domain $S$ to a range $U$ is called ($\epsilon$, $f(\epsilon)$-sensitive in the range $(a, b)$, if for any $p$, $q \in S$, with $a < D(p, q) < b$, and any $r : a < r < b$, the following condition holds in addition to conditions from Definition 1:}
\begin{itemize}
\item{$log(1/p_1) > f(\epsilon)log(1/p_2)$.}
\end{itemize}
\end{definition}

\medskip

Compared to previous work which uses nearest neighbor searches~\cite{fpga}, LSH and particularly LSHForest scales much more efficiently. The build time of a KNN model can have a complexity of $O(n^2)$, where $n$ is the number of items. In contrast, LSH construction is done in linear time and the query is a factor of the number of permutations as similarity computation is the most intensive factor~\cite{lshforest}. The theoretical complexities of operations on LSHForest are provided from the original paper in Table~\ref{table:forestcomplex}. When compared with time and space complexities for the automata in regular expression searching in Table~\ref{table:facomplex}, the logarithmic improvement and fixed storage space is clear.

\begin{table} [ht!]
\caption{Time Complexities for LSHForest~\cite{lshforest}}
\centering
\begin{tabular}{|c | c |}
\hline
\textbf{Operation} & \textbf{Complexity} \\
\hline
Insertion & $O(l * log_B n)$ \\
\hline
Deletion & $O(l * log_B n)$ \\
\hline
Query &  $O(l * log_B n) + O(l * log B) + O(M/B)$\\
\hline
\end{tabular}
\label{table:forestcomplex}
\end{table}

In these equations, $l$ represents the number of trees, $B$ the branching factor of an internal node, and $n$ the number of data points in the dataset. Storage is also optimized to be linear, $O(n)$, through the use of compressed PATRICIA tries~\cite{lshforest}. The query for some point $q$ for $m$ nearest neighbors first performs a binary search for the longest matching prefix at a leaf node with the point $s_i$. Then some $M$ points are collected synchronously across all trees and ranked by similarity score, with the top $m$ being returned as an answer. We use the $m$ number as votes to ultimately classify the sample by majority once the query is performed. This majority vote approach also allows for multiple classification results; for example, an adaptive system may be able to take the second closest result if the first choice turns out to be erroneous.
