\section{Datasets}

Instead of using a single dataset or network environment to evaluate \textsc{PALM-Forest}, we combined packet captures from several available public datasets. Our sources include the CDX 2009 captures~\cite{cdx2009}, the Skynet Tor dataset~\cite{skynet}, ISCX VPN/non-VPN and Tor/non-Tor datasets~\cite{vpn-dataset, tor-dataset}, and repositories from Wireshark, Cloudshark, and IEEE Dataport. While there is benefit to using a common dataset for comparison, investigation has shown that using captures from only a single environment can lead to bias in results~\cite{Silva2022}. A machine learning algorithm might learn characteristics of the network or environment such as IP addresses rather than actual protocol features, which can hurt generalizability of the model. For this reason and to increase the scope of classes beyond what is available in any single public dataset, we created our own repository of twenty-five different, common application layer protocol packet captures from combining several public research datasets. For experimental reproducibility and general use we have made this new dataset available publicly~\footnote{https://github.com/mayakapoor/palm} along with our source code for pre-processing and hashing.

\begin{table} [ht!]
\caption{Description of Combined Dataset}
\centering
\begin{tabular}{|c |c | c |}
\hline
\textbf{Application Type} & \textbf{Protocol} & \textbf{Number of Samples} \\
\hline
\hline
\textit{File Transfer} & FTP & 10,021 \\
& FTP-DATA & 138,068 \\
& BitTorrent &  20,648 \\
\hline
\hline
\textit{Voice-over-IP} & MGCP & 1,568 \\
& SIP & 1,118 \\
& H.225 &  1,300 \\
& RTP &  351,610 \\
\hline
\hline
\textit{Mail} & SMTP & 5,981 \\
& POP & 1,675 \\
& IMAP &  3,318 \\
\hline
\hline
\textit{Authentication \& Access} & LDAP & 1,354 \\
& SMB &  3,554 \\
& Telnet & 1,888 \\
\hline
\hline
\textit{Tunneling} & GPRS & 9,999 \\
& PPTP & 1,288 \\
& SSH & 3,039 \\
\hline
\hline
\textit{Web \& Chat} & DNS & 154,208 \\
& HTTP & 111,589 \\
& XMPP &  1,553 \\
\hline
\hline
\textit{Networking} & DHCP & 1,444 \\
& NBNS & 1,216 \\
& GQUIC &  1,740 \\
& NTP & 1,940 \\
& SSDP &  8,504 \\
& RTCP &  1,626 \\
\hline
\end{tabular}
\label{table:dataset}
\end{table}

Our selected features which we extracted from the packets were the source port number, destination port number, IP next protocol, packet length, IP differentiated services field, IP flags field, and the tokens created from the packet payload. These values were parsed from the network layer of the PCAP data using Pyshark~\cite{pyshark}, a Python library that wrappers \texttt{tshark} for packet analysis. We perform MinHashing on these values and then index and search for classification results.

Because we included a variety of traces and some protocols appear more frequently than others, some of the protocols like FTP-DATA are overwhelmingly represented, while data like H.225 are much harder to come by. To handle this class imbalance problem we have implemented random under-sampling. An advantage of under-sampling is that it will be able to correct for imbalanced data, reducing the chance of our analysis being skewed towards the majority, a phenomenon known as the accuracy paradox. We split the sampled set into two halves, one for training and one for testing.
