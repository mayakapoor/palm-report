\section{Experiments}

We posed the following research questions and performed related experiments in order to assess the effectivity of the \textsc{PALM} model:

\begin{itemize}
\item\textbf{R1. (5.1)} Given labeled training data, can packets be classified by their application-layer protocol through the construction and index of locality-sensitive hashes using header and payload contents?
\item\textbf{R2. (5.2)} Can we also distinguish traffic types (such as email or VoIP data) from one another using this model? Does it generalize to other kinds of network traffic classes besides protocol?
\item\textbf{R3. (5.3)} Can a hash embedding system uniquely identify protocols with stereotypically weak signatures that are not easily detected by regular expressions? As a specific example, can we detect RTP packets from non-RTP ones in the dataset?
\item\textbf{R4. (5.4)} For payload analysis, does using only high-value tokens as characterized by TF-IDF score improve distance measure results by isolating important features and reducing noise effectively in the data?
\item\textbf{R5. (5.5)} Does the combination of the ALPINE and PALM models, i.e. multiple hash embeddings and concatenating those results, improve classification performance over any single hash embedding of a feature set?
\item\textbf{R6. (5.6)} Are there universal features which are indicative of protocols, or do the most important features vary per protocol? Which ones are important for which protocols and are there any general rules which may be identified?
\item\textbf{R7. (5.7)} Can non-encrypted traffic be distinguished from encrypted traffic on non-standard ports (for example, SSH over port 443) using the same technique as described in R1?
\item\textbf{R8. (5.8)} Does the number of classes, hashes, or sample size affect the performance throughput or classification accuracy significantly?
\end{itemize}

For all classification experiments, we record traditional measures of precision, recall, and F1-score. We also measure Cohen's kappa ($\kappa$) which describes how often multiple raters agree on a vote in ensemble classification adjusted for the times they agree by chance. In Equation 4, the value $p_o$ is the relative observed agreement, and $p_e$ is the hypothetical probability that they agree by chance.

\begin{equation}
k = \frac{p_o - p_e}{1 - p_e}
\end{equation}

For systems experiments, we measure the throughput by taking into account the number of Megabits per second of packets the system is capable of processing. Memory usage during testing is also profiled by mebibytes using Python's native memory profiler. We also record the milliseconds per classification and training/test times for varying sample sizes.

\subsection{Protocol Auto Detection}
\begin{table}
\caption{Protocol Autodetection Results}
\centering
\begin{tabular}{| c | p{0.6cm}  p{0.6cm}  p{0.6cm} || p{0.6cm}  p{0.6cm}  p{0.6cm} |}
\hline
& & \textbf{Jaccard} & & & \textbf{TF-IDF} & \\
\hline
\hline
\textbf{Accuracy} & & 0.996 & & & 0.841 & \\
\textbf{Cohen's $\kappa$} & & 0.997 & & & 0.841 & \\
\hline
\hline
 \textbf{Protocol} & \textbf{P} & \textbf{R} & \textbf{F1} & \textbf{P} & \textbf{R} & \textbf{F1} \\
 \hline
 BitTorrent & 1.00 & 0.99 & 0.99 & 0.89 & 0.85 & 0.86 \\
 DHCP & 1.00 & 1.00 & 1.00 & 0.91 & 1.00 & 0.95 \\
 DNS & 1.00 & 0.99 & 1.00 & 0.95 & 0.97 & 0.96 \\
 FTP & 0.99 & 1.00 & 0.99 & 0.83 & 0.90 & 0.86 \\
 FTP-DATA & 1.00 & 1.00 & 1.00 & 0.93 & 0.96 & 0.94 \\
 GPRS & 1.00 & 1.00 & 1.00 & 0.91 & 0.98 & 0.95 \\
 GQUIC & 1.00 & 0.99 & 0.99 & 0.87 & 0.75 & 0.81 \\
 HTTP & 1.00 & 0.99 & 0.99 & 0.82 & 0.48 & 0.61 \\
 H.225 & 1.00 & 1.00 & 1.00 & 0.83 & 0.97 & 0.90 \\
 IMAP & 1.00 & 0.99 & 0.99 & 0.86 & 0.79 & 0.82\\
 LDAP & 1.00 & 0.99 & 0.99 & 0.87 & 1.00 & 0.93 \\
 MGCP & 1.00 & 1.00 & 1.00 & 0.90 & 0.65 & 0.75 \\
 NBNS & 0.99 & 1.00 & 0.99 & 0.94 & 1.00 & 0.97 \\
 NTP & 1.00 & 1.00 & 1.00 & 0.91 & 1.00 & 0.95 \\
 POP & 0.98 & 1.00 & 0.99 & 0.91 & 0.47 & 0.62 \\
 PPTP & 0.95 & 1.00 & 0.98 & 0.84 & 0.90 & 0.87 \\
 RTCP & 1.00 & 1.00 & 1.00 & 0.96 & 1.00 & 0.98 \\
 RTP & 1.00 & 1.00 & 1.00 & 0.89 & 0.88 & 0.89\\
 SIP & 1.00 & 1.00 & 1.00 & 0.54 & 0.98 & 0.69 \\
 SMB & 1.00 & 0.97 & 0.98 & 0.95 & 0.99 & 0.97 \\
 SMTP & 1.00 & 0.99 & 0.99 & 0.92 & 0.93 & 0.93 \\
 SSDP & 1.00 & 1.00 & 1.00 & 0.72 & 0.28 & 0.40 \\
 SSH & 1.00 & 0.98 & 0.99 & 0.90 & 0.86 & 0.88 \\
 Telnet & 0.99 & 1.00 & 0.99 & 0.86 & 0.99 & 0.92 \\
 TLS & 1.00 & 1.00 & 1.00 & 1.00 & 1.00 & 1.00 \\
 XMPP & 1.00 & 1.00 & 1.00 & 0.79 & 0.90 & 0.84 \\
 \hline
\end{tabular}
\label{table:results1}
\end{table}

The autodetection of application-layer protocols is a crucial functionality for lawful interception devices like DPI middleware boxes in order to properly assess, parse, and process the traffic they are checking. The traffic environment on any given signal can be wildly diverse; thus, any classification system must be capable of scaling to a large, multiclass problem. Previous systems~\cite{hslf, fpga} have not attempted protocol detection to the scale of a 26-class problem, making this experiment and the  \textsc{PALM} model a true and fresh pioneer in the DPI field.

To address the first question, we split the data into training and test sets and extracted the features from each of the protocols. We generated hash values for the training samples, adding them to the classifier. The results of the testing samples are provided in Table~\ref{table:results1}. The evidence shows that the combined model has high accuracy and precision/recall for protocol identifications. Cohen's kappa also shows a strong agreement among the ensemble voters. The method also performs equally well across application types, indicating generalizability.

Figure~\ref{fig:combined} shows the confusion matrix results of the classifier's ensemble voting system. The results across the combined dataset are near and in some cases are 100\%, with the only mentionable degradation being a slight tendancy to select MGCP as SSH traffic.

\begin{figure*} [ht!]
  \includegraphics[width=\textwidth]{img/f1_scores.png}
  \caption{Comparative F1-scores of using TF-IDF to adjust weight of tokens versus using all tokens to assess Jaccard similarity.}
  \label{fig:f1compare}
\end{figure*}


\subsection{Classification by Traffic Type}
RFCs and standardized documentation necessitate that there are some detectable similarities between protocols. It is reasonable to think that detection of more abstract class types like traffic type (i.e. email, file transfer, web data) would be more heterogeneous and thus more difficult to embed. We experimented with classification by traffic type by amalgamating the dataset into classes as described in Table~\ref{table:dataset} in the datasets section of this work. Results in Figure~\ref{fig:trafficresults} show a confusion matrix of the classification results which are also highly accurate.

\begin{figure} [hbt!]
  \includegraphics[width=\columnwidth]{img/traffic_class_results.png}
  \caption{Confusion matrix of traffic type classification results.}
  \label{fig:trafficresults}
\end{figure}

\subsection{RTP Detection and Weak Signatures}
RTP is difficult to detect per-packet for regular expression scanners because it has a weak signature~\cite{rfc3550}. Our hash embeddings are capable of capturing more features in the data than just a payload signature and are thus more apt to this detection problem. To test this, we labeled the data in the combined dataset as either RTP or non-RTP data and ran the classification experiment. Results in Table~\ref{table:rtpresults} show highly accurate classification results for our model.

\begin{table}
\centering
\caption{RTP Detection Results}
\begin{tabular}{| c | c  c  c|}
\hline
\textbf{Accuracy} & & 1.00  & \\
\textbf{Cohen's $\kappa$} & & 1.00 & \\
\hline
\hline
 Class & \textbf{P} & \textbf{R} & \textbf{F1} \\
\hline
 RTP & 1.00 & 1.00 & 1.00 \\
\hline
 non-RTP & 1.00 & 1.00 & 1.00 \\
\hline
\end{tabular}
\label{table:rtpresults}
\end{table}

\subsection{TF-IDF Score Evaluation}
In order to determine the effectivity of Jaccard similarity versus more complex similarity metrics, we evaluated the results of using payload tokens which were considered \textit{high-value} by TF-IDF score. We posed this question to address concern that large data payloads, for example HTML documents or email messages, may introduce noise into hashes which would reduce classification accuracy. Instead, isolating payload tokens to only the high value ones could reduce spurious extra data. In order to test this, we implemented a TF-IDF measurement of tokens in the training stage. This was done by modifying configurations of the PALM model to filter the added tokens. When training and test signatures were generated, only those tokens with TF-IDF score above the minimum threshold were kept and included in the actual LSH signature. The results in Table~\ref{table:results1} show that this actually had a negative impact on the overall result, indicating the basic Jaccard similarity was a better distance metric for this use case.

\begin{table}
\caption{Multi-Hash Embedding Classification Results}
\centering
\begin{tabular}{| p{1.5cm} | p{0.4cm} p{0.4cm} p{0.4cm} || p{0.4cm} p{0.4cm} p{0.4cm} || p{0.4cm} p{0.4cm} p{0.4cm}|}
\hline
& \textbf{ALPINE} & & & \textbf{PALM} & & & \textbf{Multi-Model} & & \\
\hline
\hline
\textbf{Accuracy} & 0.988 & & & 0.717 & & & 0.996 & & \\
\textbf{Cohen's~$\kappa$} & 0.988 & & & 0.705 & & & 0.997 & & \\
\hline
\hline
 \textbf{Protocol} & \textbf{F1} & \textbf{P} & \textbf{R} & \textbf{F1} & \textbf{P} & \textbf{R} & \textbf{F1} & \textbf{P} & \textbf{R} \\
 \hline
 BitTorrent & 0.98 & 0.97 & 1.00 & 0.74 & 0.75 & 0.73 & 0.99 & 1.00 & 0.99 \\
 DHCP & 1.00 & 1.00 & 1.00 & 0.91 & 0.83 & 1.00 & 1.00 & 1.00 & 1.00 \\
 DNS & 1.00 & 1.00 & 1.00 & 0.24 & 0.46 & 0.17 & 1.00 & 1.00 & 0.99 \\
 FTP & 0.96 & 0.97 &  0.94 & 0.90 & 0.83 & 0.98 & 0.99 & 0.99 & 1.00 \\
 FTPDATA & 1.00 & 1.00 & 1.00 & 0.20 & 0.37 & 0.13 & 1.00 & 1.00 & 1.00 \\
 GPRS & 1.00 & 1.00 & 1.00 & 0.91 & 0.98 & 0.85 & 1.00 & 1.00 & 1.00 \\
 GQUIC & 0.99 & 0.99 & 1.00 & 0.27 & 0.48 & 0.19 & 0.99 & 1.00 & 0.98 \\
 HTTP & 0.99 & 1.00 & 1.00 & 0.61 & 0.65 & 0.55 & 1.00 & 1.00 & 0.99 \\
 H.225 & 1.00 & 1.00 & 1.00 & 0.63 & 0.68 & 0.95 & 1.00 & 1.00 & 0.99 \\
 IMAP & 0.99 & 0.94 & 0.98 & 0.85 & 0.77 & 0.59 & 0.99 & 1.00 & 0.99 \\
 LDAP & 0.96 & 1.00 & 0.98 & 0.66 & 0.75 & 0.59 & 0.99 & 1.00 & 0.99 \\
 MGCP & 1.00 & 1.00 & 1.00 & 0.90 & 0.83 & 1.00 & 1.00 & 1.00 & 1.00 \\
 NBNS & 0.99 & 0.98 & 1.00 & 0.87 & 0.82 & 0.92 & 0.99 & 0.99 & 1.00 \\
 NTP & 1.00 & 1.00 & 1.00 & 0.90 & 0.82 & 0.99 & 1.00 & 1.00 & 1.00 \\
 POP & 0.99 & 0.98 & 0.99 & 0.26 & 0.41 & 0.20 & 0.99 & 0.98 & 1.00 \\
 PPTP & 0.99 & 0.98 & 0.99 & 0.49 & 0.33 & 1.00 & 0.98 & 0.95 & 1.00 \\
 RTCP & 1.00 & 1.00 & 1.00 & 0.92 & 0.85 & 1.00 & 1.00 & 1.00 & 1.00 \\
 RTP & 0.99 & 0.98 & 0.99 & 0.40 & 0.65 & 0.29 & 1.00 & 1.00 & 1.00 \\
 SIP & 1.00 & 1.00 & 1.00 & 0.93 & 0.87 & 1.00 & 1.00 & 1.00 & 1.00 \\
 SMB & 0.99 & 1.00 & 0.98 & 0.81 & 0.80 & 0.82 & 0.98 & 1.00 & 0.97 \\
 SMTP & 0.98 & 0.99 & 0.98 & 0.81 & 0.80 & 0.82 & 0.99 & 1.00 & 0.99 \\
 SSDP & 0.99 & 1.00 & 0.98 & 0.92 & 0.86 & 1.00 & 1.00 & 1.00 & 1.00 \\
 SSH & 0.95 & 0.97 & 0.92 & 0.47 & 0.67 & 0.37 & 0.99 & 1.00 & 0.98 \\
 Telnet & 0.99 & 0.98 & 1.00 & 0.80 & 0.75 & 0.85 & 0.99 & 0.99 & 1.00 \\
 TLS & 0.99 & 0.99 & 1.00 & 0.61 & 0.51 & 0.75 & 1.00 & 1.00 & 1.00 \\
 XMPP & 1.00 & 1.00 & 0.99 & 0.92 & 1.00 & 0.85 & 1.00 & 1.00 & 1.00 \\
 \hline
\end{tabular}
\label{table:embeddingresults}
\end{table}

\subsection{LSH Multi-Embeddings}

Recent work in representation learning and knowledge discovery has asserted that multiple embeddings can more efficiently capture features of heterogenous data than any single approach~\cite{rahmen}, i.e. the addage that the sum of the parts would be greater than the whole. This theory is what supports ensemble classification as well, as the concatenation of multiple classifiers and agreeance among voters in an ensemble has yielded a reduction in error and at best correlation when there is a misclassification~\cite{tumerensemble}.

We hypothesized that enabling both ALPINE, or L2-L4 header feature embedding, as well as PALM, or payload embedding, would yield the most accurate and agreed-upon classification results. In order to test this, we ran the 26-class protocol identification test against configurations of the model with just ALPINE embedding, only PALM embedding, and a final combined run. Our results as shown in Table~\ref{table:embeddingresults} demonstrate that the ALPINE embedding alone provided strong results. The PALM embedding was not ineffective but is certainly poorer-performing. Still, the combined votes of both models turned out to be the most accurate in the final tests. Furthermore, the agreeance among voters was also highest using multiple hash embeddings.

\subsection{Feature Isolation}

The diversity of protocols and standards makes creating generalizations for classification a difficult problem. Thus, we wanted to determine which features from the header, if any, were most indicative of which protocols. We ran an additional set of experiments in feature isolation and compared results for each protocol sample to address R4. Specifically, we compared results using the following feature sets:

\begin{itemize}
\item[] \textbf{Stream = } \{L4 protocol, src IP, dest IP\}
\item[] \textbf{Ports = } \{L4 protocol, src port, dest port\}
\item[] \textbf{Length = } \{length\}
\item[] \textbf{Header = } \{src IP, dest IP, src port, dest port, L4 protocol, length\}
\item[] \textbf{Payload = } \{payload\}
\end{itemize}
It is indicated by the results in Table~\ref{table:featureisolationacc} that ports were still mostly accurate in our dataset. Stream information was not as accurate as ports but was still a highly indicative feature. For some protocols like SSDP and NTP, length was also effective, but in several casses it was not an accurate similarity feature.

\begin{table}
\centering
\caption{Feature Isolation Test Results}
\begin{tabular}{| c | c | c |}
\hline
\textbf{Feature} & \textbf{Accuracy} & \textbf{Cohen's $\kappa$} \\
\hline
\hline
\textit{Stream Only} & 0.89 & 0.89 \\
\hline
\textit{Ports Only} & 0.99 & 0.99 \\
\hline
\textit{Length} & 0.74 & 0.72 \\
\hline
\textit{Payload} & 0.72 & 0.71 \\
\hline
\textit{Header} & 0.98 & 0.98 \\
\hline
\textit{Combined} & 0.99 & 0.99 \\
\hline
\end{tabular}
\label{table:featureisolationacc}
\end{table}

\subsection{Non-standard Port Usage}

\begin{table} [ht!]
\centering
\caption{Non-Standard Port Auto Detection Results}
\begin{tabular}{| c | p{0.6cm}  p{0.6cm}  p{0.6cm} | p{0.6cm}  p{0.6cm}  p{0.6cm} |}
\hline
& \textbf{Non-Encrypted} & & & \textbf{SSH} & & \\
\hline
\textbf{Accuracy} & & 0.93 & & & 0.89 & \\
\textbf{Cohen's $\kappa$} & & 0.87 & & & 0.78 & \\
\hline
\hline
 \textbf{Protocol} & \textbf{P} & \textbf{R} & \textbf{F1} & \textbf{P} & \textbf{R} & \textbf{F1} \\
 \hline
 TLS & 0.97 & 0.90 & 0.93 & 0.97 & 0.80 & 0.88 \\
 non-TLS & 0.90 & 0.97 & 0.94 & 0.90 & 0.97 & 0.90 \\
 \hline
\end{tabular}
\label{table:results2}
\end{table}

Our next experiment was to assess the ability of the model to determine traffic on non-standard ports. An example use case would be to determine if traffic on port 443 are truly encrypted packets (TLS) or not. In order to simulate this, we removed port numbers from our hash calculation. We created two classes of encrypted and non-encrypted traffic. We performed random under-sampling and ran the model, generating hash values of the two traffic types and confusion matrices of the results in Figure~\ref{fig:tlscm} and Figure~\ref{fig:tlsssh}. Results from the test samples are provided in Table~\ref{table:results2} which also shows the ability of the model to distinguish SSH in encrypted traffic streams. This would be a useful task, for example, in filtering port 443 for attackers attempting to connect through SSH in firewalls which default-allow port 443.

\begin{figure} [hbt!]
  \includegraphics[width=\columnwidth]{img/tls_plot.png}
  \caption{A confusion matrix of the non-standard port autodetection results.}
  \label{fig:tlscm}

  \includegraphics[width=\columnwidth]{img/tls_ssh.png}
  \caption{A confusion matrix of the non-standard port autodetection results with \textsc{PALM-Forest} also configured to identify SSH traffic.}
  \label{fig:tlsssh}
\end{figure}

\subsection{Model Performance and Throughput}
One critical requirement for applied machine learning in network processing is that systems must keep up with signal processing at very high rates. Any passive system must not interfere with legitimate traffic and service. Furthermore, the software must be capable of performing the necessary processing on as much of the traffic as possible (ideally, all of it). While thinning and load-balancing solutions as well as diverting and copying traffic for offline analysis can help levy this concern, there is still the desire to employ classification solutions at real-time rates. Thus, we perform experiments to see how the \textsc{PALM} system scales based on model sizes and number of classes. We measured the training time, system throughput in millibits per second (Mbps) as well as memory usage during the testing phase in megibytes (MiB).

We ran all performance tests on the combined dataset consisting of 140,157 total packets. In order to avoid bias we implemented random under-sampling to even out the distribution of data, causing our total number of packets after sampling to be a fraction from the original set. For testing the number of classes, we created experiments with binary RTP/non-RTP classification, the traffic-type multiclass experiment with 8 classes/types, and the largest 26-class experiment where we identify all possible data layer protocols. In Table~\ref{table:performanceresults}, we detail the results of the experiments for a varied number of classes and sample sizes.

\begin{table*}
\caption{Performance Results for Varied Number of Classes}
\centering
\begin{tabular}{| c | c | c | c | c | c | c | c |}
\hline
Classes & \# Hashes & \# Test Samples & Training Time & Test Time & Memory Usage & Throughput & Accuracy \\
\hline
\hline
\textbf{RTP/non-RTP} & 20,613 & 13,743 & 4.273s & 1.21ms & 470.8 MiB & 2.596 Mb\/s & 1.000 \\
\textit{Binary classification} & 2,400 & 1,600 & 0.738s & 0.866ms & 283.7 MiB & 3.139 Mb\/s & 0.997 \\
& 240 & 160 & 0.075s & 1.093ms & 353.4 MiB & 2.317 Mb\/s & 1.00 \\
\hline
\textbf{Traffic Type} & 28,543 & 19,029 & 7.136s & 1.015ms & 584.6 MiB & 3.728 Mb\/s & 0.997 \\
\textit{8-class problem} & 12,600 & 8,400 & 3.31s & 1.006ms & 305.3 MiB & 2.842 Mb\/s & 0.997 \\
& 240 & 160 & 0.683s & 2.397ms & 290.6 MiB & 2.54 Mb\/s & 0.998 \\
\hline
\textbf{Protocol} & 17,347 & 11,565 & 5.312s & 1.138ms & 352.4 MiB & 3.041 Mb\/s & 0.997 \\
\textit{26-class problem} & 15,600 & 10,400 & 3.13s & 0.727ms & 305.3 MiB & 4.231 Mb\/s & 0.998 \\
& 1,560 & 1,040 & 0.492s & 0.901ms & 301.8 MiB & 3.212 Mb\/s & 0.994 \\
\hline
\end{tabular}
\label{table:performanceresults}
\end{table*}

\begin{figure*} [hbt!]
  \includegraphics[width=\textwidth]{img/combined.png}
  \caption{We attempted a twenty-six class identification problem with near perfect results using the PALM-ALPINE combined model.}
  \label{fig:combined}
\end{figure*}
