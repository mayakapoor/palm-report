\section{Experiments and Results}

We posed the following four research questions and performed four related experiments in order to assess the effectivity of the \textsc{PALM-Forest} model:

\begin{itemize}
\item\textbf{R1.} Given labeled training data, can packets be classified by their application-layer protocol through the construction and index of locality-sensitive hashes using header and payload contents?
\item\textbf{R2.} Can non-encrypted traffic be distinguished from encrypted traffic on non-standard ports (for example, SSH over port 443) using the same technique as described in R1?
\item\textbf{R3.} Does using only high-value tokens as characterized by TF-IDF score improve distance measure results by isolating important features and reducing noise effectively in the data?
\item\textbf{R4.} Are there universal features which are indicative of protocols, or do the most important features vary per protocol? Which ones are important for which protocols and are there any general rules which may be identified?
\end{itemize}

\begin{figure*} [hbt!]
  \includegraphics[width=\textwidth]{img/proto_plot.png}
  \caption{A confusion matrix of the protocol classification results.}
  \label{fig:protocm}
\end{figure*}

We use measures of precision, recall, and F1 score for assessment. Our measure of recall is the number of packets correctly classified as a particular protocol $P$ divided by the number of packets correctly classified plus packets incorrectly classified as not $P$~\cite{Zhang}.

\[\text{Recall} = \frac{TP}{TP+FN}\] \par

Precision is defined as the number of packets correctly classified as a particular protocol $P$ divided by the sum of the number of packets correctly classified plus the number of packets incorrectly classified as $P$ \cite{Zhang}.

\[\text{Precision} = \frac{TP}{TP+FP}\] \par

F1 score is the harmonic mean between precision and recall.

\[\text{F1} = 2 \times \frac{\text{Precision $\times$ Recall}}{\text{Precision + Recall}}\] \par
\vspace{\baselineskip}

We also measure Cohen's kappa ($\kappa$) which describes how often multiple raters agree on a vote in ensemble classification adjusted for the times they agree by chance. In Equation 4, the value $p_o$ is the relative observed agreement, and $p_e$ is the hypothetical probability that they agree by chance.

\begin{equation}
k = \frac{p_o - p_e}{1 - p_e}
\end{equation}

To address the first question, we split the data into training and test sets and extracted the features from each of the protocols. We generated hash values for the training samples, adding them to the classifier. The results of the testing samples are provided in Table~\ref{table:results1}. The evidence shows that \textsc{PALM-Forest} has high accuracy and precision/recall for almost all protocol identifications. Cohen's kappa also shows a strong agreement among the ensemble voters. The method also performs equally well across application types, indicating generalizability for this method. Figure~\ref{fig:protocm} shows a confusion matrix of the results.

\begin{table}
\caption{Protocol Autodetection Results}
\centering
\begin{tabular}{| c | p{0.6cm}  p{0.6cm}  p{0.6cm} || p{0.6cm}  p{0.6cm}  p{0.6cm} |}
\hline
& & \textbf{Jaccard} & & & \textbf{TF-IDF} & \\
\hline
\hline
\textbf{Accuracy} & & 0.94 & & & 0.84 & \\
\textbf{Cohen's $\kappa$} & & 0.94 & & & 0.84 & \\
\hline
\hline
 \textbf{Protocol} & \textbf{P} & \textbf{R} & \textbf{F1} & \textbf{P} & \textbf{R} & \textbf{F1} \\
 \hline
 BitTorrent & 0.95 & 0.98 & 0.97 & 0.89 & 0.85 & 0.86 \\
 DHCP & 0.98 & 1.00 & 0.99 & 0.91 & 1.00 & 0.95 \\
 DNS & 0.97 & 0.97 & 0.97 & 0.95 & 0.97 & 0.96 \\
 FTP & 0.93 & 0.98 & 0.96 & 0.83 & 0.90 & 0.86 \\
 FTP-DATA & 0.96 & 0.95 & 0.96 & 0.93 & 0.96 & 0.94 \\
 GPRS & 0.98 & 0.99 & 0.99 & 0.91 & 0.98 & 0.95 \\
 GQUIC & 0.91 & 0.78 & 0.84 & 0.87 & 0.75 & 0.81 \\
 HTTP & 0.96 & 0.85 & 0.90 & 0.82 & 0.48 & 0.61 \\
 H.225 & 0.96 & 1.00 & 0.98 & 0.83 & 0.97 & 0.90 \\
 IMAP & 0.90 & 0.96 & 0.93 & 0.86 & 0.79 & 0.82\\
 LDAP & 0.96 & 1.00 & 0.98 & 0.87 & 1.00 & 0.93 \\
 MGCP & 0.96 & 1.00 & 0.98 & 0.90 & 0.65 & 0.75 \\
 NBNS & 0.96 & 1.00 & 0.98 & 0.94 & 1.00 & 0.97 \\
 NTP & 0.97 & 1.00 & 0.98 & 0.91 & 1.00 & 0.95 \\
 POP & 0.95 & 0.71 & 0.81 & 0.91 & 0.47 & 0.62 \\
 PPTP & 0.95 & 1.00 & 0.97 & 0.84 & 0.90 & 0.87 \\
 RTCP & 0.98 & 1.00 & 0.99 & 0.96 & 1.00 & 0.98 \\
 RTP & 0.96 & 0.88 & 0.92 & 0.89 & 0.88 & 0.89\\
 SIP & 0.84 & 0.99 & 0.91 & 0.54 & 0.98 & 0.69 \\
 SMB & 0.95 & 0.99 & 0.97 & 0.95 & 0.99 & 0.97 \\
 SMTP & 0.95 & 0.96 & 0.95 & 0.92 & 0.93 & 0.93 \\
 SSDP & 0.98 & 1.00 & 0.99 & 0.72 & 0.28 & 0.40 \\
 SSH & 0.93 & 0.90 & 0.92 & 0.90 & 0.86 & 0.88 \\
 Telnet & 0.94 & 0.99 & 0.97 & 0.86 & 0.99 & 0.92 \\
 XMPP & 0.94 & 1.00 & 0.97 & 0.79 & 0.90 & 0.84 \\
 \hline
\end{tabular}
\label{table:results1}
\end{table}

\begin{table} [ht!]
\centering
\caption{Non-Standard Port Auto Detection Results}
\begin{tabular}{| c | p{0.6cm}  p{0.6cm}  p{0.6cm} |}
\hline
\textbf{Accuracy} & & 0.93 & \\
\textbf{Cohen's $\kappa$} & & 0.87 & \\
\hline
\hline
 \textbf{Protocol} & \textbf{P} & \textbf{R} & \textbf{F1} \\
 \hline
 TLS & 0.97 & 0.90 & 0.93  \\
 non-TLS & 0.90 & 0.97 & 0.94 \\
 \hline
\end{tabular}
\label{table:results2}
\end{table}

\begin{table} [ht!]
\centering
\caption{SSH Detection in Encrypted Traffic Stream Results}
\begin{tabular}{| c | p{0.6cm}  p{0.6cm}  p{0.6cm} |}
\hline
\textbf{Accuracy} & & 0.89 & \\
\textbf{Cohen's $\kappa$} & & 0.78 & \\
\hline
\hline
 \textbf{Protocol} & \textbf{P} & \textbf{R} & \textbf{F1} \\
 \hline
 TLS & 0.97 & 0.80 & 0.88  \\
 SSH & 0.90 & 0.97 & 0.90 \\
 \hline
\end{tabular}
\label{table:results3}
\end{table}

Our second experiment was to assess the ability of the model to determine traffic on non-standard ports. An example use case would be to determine if traffic on port 443 are truly encrypted packets (TLS) or not. In order to simulate this, we removed port numbers from our hash calculation and gathered an additional mixed dataset of TLS-encrypted traffic (also provided in our public dataset). We combined the non-encrypted samples from the previous experiment and thus created two classes of encrypted and non-encrypted traffic. We performed random under-sampling and ran the model, generating hash values of the two traffic types. Results from the test samples are provided in Table~\ref{table:results2} along with a confusion matrix in Figure~\ref{fig:tlscm}. We also performed another test to classify SSH packets specifically from TLS and non-TLS packets, and show the results in Figure~\ref{fig:tlsssh}. Table~\ref{table:results3} shows the ability of the model to distinguish SSH in encrypted traffic streams. This would be a useful task, for example, in filtering port 443 for attackers attempting to connect through SSH in firewalls which default-allow port 443.

\begin{figure} [hbt!]
  \includegraphics[width=\linewidth]{img/tls_plot.png}
  \caption{A confusion matrix of the non-standard port autodetection results.}
  \label{fig:tlscm}
\end{figure}

\begin{figure} [hbt!]
  \includegraphics[width=\linewidth]{img/tls_ssh.png}
  \caption{A confusion matrix of the non-standard port autodetection results with \textsc{PALM-Forest} also configured to identify SSH traffic.}
  \label{fig:tlsssh}
\end{figure}

\begin{figure*} [h!]
  \includegraphics[width=\textwidth]{img/f1_scores.png}
  \caption{Comparative F1-scores of using TF-IDF to adjust weight of tokens versus using all tokens to assess Jaccard similarity.}
  \label{fig:f1compare}
\end{figure*}

Our third research question was posed to address concern that large data payloads, for example HTML documents or email messages, may introduce noise into hashes which would reduce classification accuracy. Instead, isolating payload tokens to only the high value ones could reduce spurious extra data. In order to test this, we implemented a TF-IDF measurement of tokens in the training stage. When training and test signatures were generated, only those tokens with TF-IDF score above the minimum threshold were kept and included in the actual LSH signature.

Figure~\ref{fig:f1compare} shows that reducing the number of payload tokens overall actually negatively impacted the accuracy of the model. In every case, the F1 score was much lower and overall accuracy was reduced to 84\%. Because of the hash-based nature of the mechanism, there is little effective gain in reducing the number of tokens in terms of storage space because it is stored together as the hash in either case. For this reason, the more naïve approach of splitting and hashing the whole payload is the optimal method.

Some protocols are better identified by header features versus payload features, and vice versa. The diversity of protocols and standards makes creating generalizations for classification a difficult problem. In order to better understand what features were most indicative of which protocols, we ran a fourth set of experiments in feature isolation and compared results for each protocol sample to address R4. Specifically, we compared results using the following feature sets:

\begin{itemize}
\item[] \textbf{Nonstandard Ports + Payload:} \{L4 Protocol, DS Field, IP Flags, Length, Payload\}
\item[] \textbf{Ports:} \{Source Port, Destination Port\}
\item[] \textbf{Header:} \{DS Field, IP Flags, Length\}
\item[] \textbf{Flags:} \{DS Field, IP Flags\}
\item[] \textbf{Length:} \{Length\}
\item[] \textbf{Payload:} \{Payload\}
\end{itemize}

 Interestingly, SIP classification improved using only payload features and no header features. FTP-DATA, NBNS, and RTCP were also as accurate or very nearly as accurate with just header features. This is particularly useful knowledge for FTP-DATA as the payload isolation proved to be completely uninformative in our isolation test. A comparative visual of the results from this experiment is shown in Figure~\ref{fig:featureisolation}.

\begin{figure*} [h!]
  \includegraphics[width=\textwidth]{img/featureisolation.png}
  \caption{Comparative F1-scores for protocols when performing classification using feature isolation.}
  \label{fig:featureisolation}
\end{figure*}

\begin{table} [ht!]
\centering
\caption{Feature Isolation Test Results}
\begin{tabular}{| c | c | c |}
\hline
\textbf{Feature} & \textbf{Accuracy} & \textbf{Cohen's $\kappa$} \\
\hline
\hline
\textit{Non-Standard Ports + Payload} & 0.88 & 0.88 \\
\hline
\textit{Ports} & 1.00 & 1.00 \\
\hline
\textit{Header} & 0.7 & 0.69 \\
\hline
\textit{Flags} & 0.27 & 0.26 \\
\hline
\textit{Length} & 0.54 & 0.52 \\
\hline
\textit{Payload} & 0.72 & 0.71 \\
\hline
\end{tabular}
\label{table:featureisolationacc}
\end{table}
