\section{Background and Related Work}

\subsubsection*{\textit{Deep Packet Inspection}}
Deep packet inspection is the process of analyzing traffic data as it comes across the network. Packet headers contain information which can tell what type of application layer data is being transported in the packet.

One example of important header information is port numbers. Port numbers are assigned by the Internet Assign Number Authority (IANA)\cite{iana} and originally were used to identify the application layer protocol payload content belonged to. In modern architectures that employ nonstandard port usage and translation, classification by port number can be inaccurate. Especially in tunneled environments, network address port translation (NAPT) can be used to map applications to unused port numbers which do not match the standard assignment\cite{Smith}. Traffic may also be routed to certain ports in order to evade firewalls or avoid detection.

One port which falls victim to spoofing most routinely is port 443 which is registered for encrypted HTTPS traffic. Attackers may send non-HTTPS traffic (SSH sessions or malware) via this port in order to avoid detection\cite{gigamon}. An insecure firewall system will allow this traffic without validating that it is indeed HTTPS. In addition to active adversarial attack, port spoofing can be used to send illegal or intelligence information which senders wish to elude detection and inspection\cite{firewall}.

In order to more accurately determine what type of traffic is flowing through the network, industry standard has evolved past the header to search for signatures in packet payloads. For example, the regular expression signature \textit{\textasciicircum(\textbackslash+ok .*pop)} can be used to match POP3 payloads~\cite{VinothGeorge}. While widely used for packet scanning, regular expression matching requires the construction of complex data structures such as deterministic and/or non-deterministic finite automata (NFA/DFA) and can be both memory and computation-intensive depending on the size of the regular expression dictionary and the complexity of the signatures themselves. DFAs are fast to search, but are prone to state explosion as every possibility is explicity built into the data structure. NFAs provide storage space that is linear to the size of the regular expression ruleset, but it is relatively easy to arrive at worst-case search performance with rule complexity~\cite{Fu}. Table~\ref{table:facomplex} shows the processing complexity and storage costs of these structures where $m$ is a number of regular expressions of length $n$.

\begin{table} [ht!]
\caption{Worst-Case Space and Time Complexities for NFA and DFA~\cite{Yu}}
\centering
\begin{tabular}{|c | c | c|}
\hline
\textbf{Data Structure} & \textbf{Processing Complexity} & \textbf{Storage Cost} \\
\hline
NFA & $O(n^{2}m)$ & $O(nm)$ \\
\hline
DFA & $O(1)$ & $O(\Sigma^{nm})$ \\
\hline
\end{tabular}
\label{table:facomplex}
\end{table}

Recently, search engines have been designed which use a hybrid approach to try to improve search performance; however, improvements on computational complexity can still be prone to memory issues in the case of large regular expression rulesets\cite{hyperscan}. Parallel computing may alleviate some of the computational stress, but is acknowledged as a brute force approach~\cite{Fu}.

In addition to per-packet solutions, there is also an abundance of research in traffic classification using machine-learning techniques that study time and flow-based features of packet streams~\cite{Salman, Cao, Lim, LiZ, Song2019, icsx-vpn-paper, iscx-tor-paper}; however, in the testing phase network engineers may not be able to wait for or buffer entire packet streams before they can classify packets and send them to follow-on processing. Thus, per-packet solutions for both encrypted and non-encrypted data are a pressing research need regardless of flow-based solutions.

\subsubsection*{\textit{Natural Language Processing}}

Similarity search is a generalized term in data mining which refers to searching for objects where the available comparator is some common pattern or similarities among them. Use for this technique includes building citation networks of similar documents~\cite{simsearch}. One of the challenges in data engineering is determining how to construct suitable, unique \textit{tokens} which characterize the data meaningfully.

Whitespace is often used as a delimiter in string search. The sentence, ``Palm trees are native to the Pacific", may be split into the set of tokens $T = \{\text{``palm"}, \text{``trees"}, \text{``are"}, \text{``native"}, \text{``to"}, \text{``the"}, \text{``pacific"}\}$. It is obvious that tokens like \text{``the"} are far too generic in the English language to be characteristic of this sentence, but a token like \text{``palm"} or \text{``pacific"} will be much more unique and indicate a stronger similarity. This idea may be more solidly quantified through taking the inverse document frequency (IDF) of tokens which minimizes the importance of terms which appear frequently in the document set. When combined with term frequency (TF) as the TF-IDF value~\cite{tf-idf}, the most relevant tokens may be found which are both common and characteristically unique of the document set.

\begin{equation}
IDF(t, D) = log\left(\frac{N}{count (d \in D : t \in d)}\right)
\end{equation}

In Equation 1, the inverse document frequency of term $t$ in document set $D$ is the logarithmically scaled, inverse fraction of the documents $d$ that contain the term $t$. The term frequency is the relative frequency of the term in the document, and their product is the combined TF-IDF value.

TF-IDF is typically used to convert texts into vector representations whose cosine similarity may be computed and compared. The TF-IDF model has been used to successfully distinguish between benign data and malicious network traffic carrying worms, executing DoS attacks, and spreading spam and nefarious content~\cite{subba2021tfidfvectorizer, blue2020distributed}. The method has also been used to distinguish darknet traffic~\cite{tfidf-darknet, Ashwini}.

%Another NLP solution to traffic classification is Packet2Vec~\cite{packet2vec}. Like TF-IDF, the Word2Vec-based model essentially computes vectors and measures their cosine similarity; however, it uses either a continuous bag of words (CBOW) or continuous skip-gram model to embed semantic context of the window of surrounding words~\cite{packet2vec}. Thus, phrases like ``palm tree" and ``palm of the hand" will have different semantic context, embedding, and similarity.

\subsubsection*{\textit{Data Mining and Frequent Item Sets}}
A more naïve approach to finding similar items between sets is to take the intersection of two sets over their union. This method is known as Jaccard similarity~\cite{mmds}, and is \textsc{PALM-Forest}'s measurement of choice:

\begin{equation}
J(X, Y) = \frac{|X \cap Y|}{|X \cup Y|}
\end{equation}

The MinHash algorithm uses Jaccard similarity, and has been purposed to estimate near-duplicate documents by set intersection~\cite{minhash}. MinHash uses a characteristic matrix to create one-hot encoded representations of the feature sets. In this matrix, a $1$ indicates the presence of the given element in the set, while $0$ indicates absence. The first step to MinHash is taken by generating a permutation of the rows of this matrix. The MinHash of a given column is the number of the first row in permuted order whose value is $1$. The sequence is continued down the columns and repeated for $k$ permutations. The probability that the MinHash function for a random permutation of rows produces the same value for two sets is a close approximation to their Jaccard similarity~\cite{mmds}.

\begin{equation}
J(P_A, P_B) = \frac{|T_A \cap T_B|}{|T_A \cup T_B|}
\end{equation}

In terms of Jaccard measure, two packets may be described as similar as shown in Equation 3 where $T$ represents the set of tokens.

Other machine learning and data mining techniques have been developed which allow for automated pattern detection in big text data. This has been applied in networking to packet payloads for the purpose of automatically generating regular expression signatures. Shim et al developed SigBox which uses the Apriori frequent item set algorithm to find frequent tokens in packets for application and protocol identification~\cite{sigbox}. RExACtor also uses Apriori combined with string sequence alignment to automatically generate regular expression signatures~\cite{rexactor}.

\subsubsection*{\textit{Locality Sensitive Hashing}}
Contrary to cryptographic hashes which attempt to avoid collision, locality-sensitive hashes preserve the similarity of data points (in our case, token sets) to one another. The following definition intuitively states that data points which are locally nearby have a higher probability of collision than further points.

\medskip

\begin{definition}
\textit{~\cite{lshforest} A family $H$ of functions from a domain $S$ to a range $U$ is called ($r$, $\epsilon$, $p_1$, $p_2$)-sensitive, with $r$, $\epsilon > 0$, $p_1 > 0$, $p_2 > 0$, if for any $p$, $q \in S$, the following conditions hold:}
\begin{itemize}
\item{\textit{if $D(p, q) \leq r$ then $Pr_H[h(p) = h(q)] \geq p_1$,}}
\item{\textit{if $D(p, q) > r(1 + \epsilon)$ then $Pr_H[h(p) = h(q)] \leq p_2$.}}
\end{itemize}
\end{definition}

\medskip

One way in which locality-sensitive hashing has been applied in the network security domain is in user-level browser fingerprinting. In this method, web-based browser fingerprints created by extracting multiple values from the browser API may be hashed using MinHash or similar functions to generate signatures of high entropy, where the data is uniquely identifiable as a particular device or host~\cite{browser}. We apply this concept similarly to network packet data, aiming for a locality-sensitive hash using features which should also evidence values unique to the protocol we are attempting to auto-detect.

Other research in LSH for packet classification include Tang et al, who proposed HSLF~\cite{hslf}, an HTTP header sequence-based LSH fingerprint generator for classifying applications in HTTP traffic using Hamming distance between the fingerprints as the distance measure. While this work is limited to only cleartext HTTP traffic, results show the ability of their SimHash-based method to accurately distinguish between data such as Firefox, VMWare, and WeChat apps in this one protocol. LSH has been applied also to identify Internet of Things (IoT) devices in a given network from other traffic flows~\cite{charyyev}. We also expand the work of Jiang and Gokhale~\cite{fpga} who showed the ability of locality-sensitive hashing to accurately classify network traffic into multi-media versus web classes using packet-level features (IP protocol, port numbers, and packet size) exclusively in order to achieve stateless packet inspection.
